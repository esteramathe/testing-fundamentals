public class StringUtils {
    public static Double convertToNumber(String number) {
        //Din String "1990" --> Double 1990d
        if (number == null) {
            return null;
        }
        return Double.valueOf(number);
    }

    public static String getStringInCapitalLetters(String text) {
        //"java" --> "JAVA"
        if (text != null) {
            return text.toUpperCase();
        }
        return null;
    }

    public static boolean isNullOrBlank(String word) {
        //blank--> "  "      empty-->""        "    java    "
        return word == null || word.trim().length() == 0;
    }

    public static String getDefaultIfNull(String text, String defaultText) {
        // if(text==null){
        // return defaultText;
        // }else return text;
        return text == null ? defaultText : text;//operator ternar
    }

    //{"java", "php", null,...}
    public static String concat(String[] strings) {
        String returnedString = null;
        if (strings != null && strings.length > 0) {
            StringBuilder sb = new StringBuilder();//constructor ce declara un stringbuilder gol
            for (int i = 0; i < strings.length; i++) {
                String myStringFromArray = strings[i];
                if (myStringFromArray != null) { //daca stringul e diferit de null, il adauga in array
                    sb.append(myStringFromArray); //cu linia 37 si 38 ne asiguram ca nu concatenam nulluri
                }

            }
            returnedString = sb.toString();

        }
        return returnedString;
    }
}
