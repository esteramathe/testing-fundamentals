import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringUtilsTest {
    @Test
    public void convertToNumberTest() {
        //given
        String testNumber = "1990";
        Double expectedResult = 1990d;
        Double unexpectedResult = 1991d;
        //when
        Double actualResult = StringUtils.convertToNumber(testNumber);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unexpectedResult, actualResult);
        assertNotNull(actualResult);
    }

    @Test
    public void convertToNumberFromNullTest() {
        //given
        String testNumber = null;
        Double expectedResult = null;
        Double unexpectedResult = 1991d;
        //when
        Double actualResult = StringUtils.convertToNumber(testNumber);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unexpectedResult, actualResult);
        assertNull(actualResult);
    }

    @Test
    public void getStringInCapitalLettersTest() {
        //given
        String givenText = "java";
        String expectedResult = "JAVA";
        //when
        String actualResult = StringUtils.getStringInCapitalLetters(givenText);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotNull(actualResult);
    }

    @Test
    public void getStringInCapitalLettersFromNullTest() {
        //given
        String givenText = null;
        String expectedResult = null;
        //when
        String actualResult = StringUtils.getStringInCapitalLetters(givenText);
        //then
        assertEquals(expectedResult, actualResult);
        assertNull(actualResult);
    }

    @Test
    public void isNullOrBlankWithBlankStringTest() {
        //given
        String givenText = "  ";
        boolean expectedResult = true;
        boolean unExpectedResult = false;
        //when
        boolean actualResult = StringUtils.isNullOrBlank(givenText);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unExpectedResult, actualResult);
        assertTrue(expectedResult);
        assertFalse(unExpectedResult);
    }

    @Test
    public void isNullOrBlankWithEmptyStringTest() {
        //given
        String givenText = "";
        boolean expectedResult = true;
        boolean unExpectedResult = false;
        //when
        boolean actualResult = StringUtils.isNullOrBlank(givenText);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unExpectedResult, actualResult);
        assertTrue(expectedResult);
        assertFalse(unExpectedResult);
    }

    @Test
    public void isNullOrBlankWithNullTest() {
        //given
        String givenText = null;
        boolean expectedResult = true;
        boolean unExpectedResult = false;
        //when
        boolean actualResult = StringUtils.isNullOrBlank(givenText);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unExpectedResult, actualResult);
        assertTrue(expectedResult);
        assertFalse(unExpectedResult);
    }

    public void isNullOrBlankWithStringTest() {
        //given
        String givenText = "  Java ";
        boolean expectedResult = false; //giventext-ul meu trece prin verificare in conditia:return word == null || word.trim().length() == 0 si atunci rezulta ca false || false intoarce false
        boolean unExpectedResult = true;
        //when
        boolean actualResult = StringUtils.isNullOrBlank(givenText);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unExpectedResult, actualResult);
        assertTrue(expectedResult);
        assertFalse(unExpectedResult);
    }

    @Test
    public void getDefaultIfNull() {
        //given
        String givenText = "text";
        String givenDefaultText = "default text";
        String expectedResult = "text";
        String unExpectedResult = " text ";
        //when
        String actualResult = StringUtils.getDefaultIfNull(givenText, givenDefaultText);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unExpectedResult, actualResult);
        assertNotNull(actualResult);
    }

    @Test
    public void getDefaultIfNullWithNullValueTest() {
        //given
        String givenText = null;
        String givenDefaultText = "default text";
        String expectedResult = givenDefaultText;
        String unExpectedResult = " text ";
        //when
        String actualResult = StringUtils.getDefaultIfNull(givenText, givenDefaultText);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unExpectedResult, actualResult);
        assertNotNull(actualResult);
    }
    @Test
    public void concatWithNullTest(){
        //given
        String [] givenStringArray=null;
        String expectedResult=null; //nu mai intra conditia in if pentru ca e null;
        String unExpectedResult="";
        //when
        String actualResult=StringUtils.concat(givenStringArray);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unExpectedResult, actualResult);
        assertNull(actualResult);
    }
    @Test
    public void concatWithStringArrayTest(){
        //given
        String [] givenStringArray={"hello"," from ","array","!"};
        String expectedResult="hello from array!";
        String unExpectedResult="hello";

        //when
        String actualResult=StringUtils.concat(givenStringArray);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unExpectedResult, actualResult);
        assertNotNull(actualResult);
    }
    @Test
    public void concatWithStringArrayHavingANullValueTest(){
        //given
        String [] givenStringArray={"hello", null ," from ", null ," array","!"};
        String expectedResult="hello from array!";
        String unExpectedResult="hello";

        //when
        String actualResult=StringUtils.concat(givenStringArray);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unExpectedResult, actualResult);
        assertNotNull(actualResult);
    }
    @Test
    public void assertSameTest() {
        String s1 = "abc";
        String s2 = "abc";
        String s3 = new String("abc");

        assertSame(s1, s2);
        assertNotSame(s1, s3);
    }
}

