import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyFirstJunitClass {
    //1.orice metoda de test trebuie sa fie adnotata cu @Test
    //2.orice metoda de test trebuie sa fie void (sa nu returneze nimic)
    //3.orice metoda de test adnotata cu @Test, nu primeste parametri
    //4.orice metoda de test trebuie sa fie publica!
    @Test
    public void myFirstJunitTest() {
        System.out.println("hello from junit test");
    }

    @Test
    public void myFirstAssertEqualsTest() {
        assertEquals(3, 3);
        assertEquals("java", "java");
        assertNotEquals(4, 3);
    }

    @Test
    public void myFirstAssertTrueFalseTest() {
        assertTrue(3 == 3);
        assertFalse(3 != 3);
    }

    @Test
    public void myFirstAssertNullTest() {
        String name = null;
        String java = "java";
        assertNull(name);
        assertNotNull(java);
    }

}