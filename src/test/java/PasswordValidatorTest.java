import org.assertj.core.api.Assert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
 //Test Driven Development
public class PasswordValidatorTest {
    @Test
    public void isValidTest() {
        //given
        String givenPassword="password123";
        //when
       Boolean actualResult=PasswordValidator.isValid(givenPassword);
        //then
        Assertions.assertThat(actualResult).isTrue();
    }
}
