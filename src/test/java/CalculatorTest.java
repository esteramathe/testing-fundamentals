import org.junit.jupiter.api.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    @BeforeAll
    public static void logBeforeAllTests() {
        System.out.println("Tests ended at: " + DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:ms").format(LocalDateTime.now()));
    }

    @AfterAll
    public static void logAfterAllTests() {
        System.out.println("Tests ended at: " + DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:ms").format(LocalDateTime.now()));
    }

    @BeforeEach
    public void setUp() {
        Calculator calculator=new Calculator();
        System.out.println("This runs before each test");
    }

    @AfterEach
    public void afterEachTest() {
        System.out.println("This runs after each test");
    }



    @Test
    public void addTest() {
        //given
        Calculator calculator = new Calculator();
        double firstNumber = 10;
        double secondNumber = 20;
        Double expectedResult = 30d;
        Double unexpectedResult = 40d;
        //when
        Double actualResult = calculator.add(firstNumber, secondNumber);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unexpectedResult, actualResult);
    }

    @Test
    public void substractTest() {
        //given
        Calculator calculator = new Calculator();
        double firstNumber = 15;
        double secondNumber = 28;
        Double expectedResult = -13d;
        Double unexpectedResult = 13d;
        //when
        Double actualResult = calculator.substract(firstNumber, secondNumber);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unexpectedResult, actualResult);

    }

    @Test
    public void substractionWithNegativeValueTest() {
        //given
        Calculator calculator = new Calculator();
        double firstNumber = 17;
        double secondNumber = -5;
        Double expectedResult = null;
        //when
        Double actualResult = calculator.substract(firstNumber, secondNumber);
        //then
        assertEquals(expectedResult, actualResult);
        Assertions.assertNull(actualResult);
    }

    @Test
    public void multiplyTest() {
        //given
        Calculator calculator = new Calculator();
        double firstNumber = 10.1;
        double secondNumber = 20;
        Double expectedResult = 202d;
        Double unexpectedResult = 205d;
        //when
        Double actualResult = calculator.multiply(firstNumber, secondNumber);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unexpectedResult, actualResult);
    }

    @Test
    @DisplayName("This is the unit test for divided method that takes into consideration division with 0")
    public void divideWithZeroTest() {
        //given
        Calculator calculator = new Calculator();
        double firstNumber = 98;
        double secondNumber = 0;
        Double expectedResult = null;
        Double unexpectedResult = 40d;
        //when
        Double actualResult = calculator.divide(firstNumber, secondNumber);
        //then
        assertEquals(expectedResult, actualResult);
        assertNotEquals(unexpectedResult, actualResult);
        assertNull(actualResult);
    }

    @Test
    public void reverseSignTest() {
        //given
        Calculator calculator = new Calculator();
        int number = 10;
        Integer expectedResult = -10;
        Integer unexpectedResult = 10;
        //when
        Integer actualResult = calculator.reverseSign(number);
        //then
        Assertions.assertEquals(expectedResult, actualResult);
        assertNotEquals(unexpectedResult, actualResult);

    }
}
